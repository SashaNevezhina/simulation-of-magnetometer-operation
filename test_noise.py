# -*- coding: utf-8 -*-
"""
Created on Sat May 30 10:03:15 2020

@author: enp
"""


import pickle
import numpy
import matplotlib.pyplot as plt
import time

with open('sx_sy_noise_0.pkl','rb') as data_without_noise:
    sx_sy_0 = pickle.load(data_without_noise)

with open('sx_sy_noise_1.pkl','rb') as data_noise1:
    sx_sy_1 = pickle.load(data_noise1)

with open('sx_sy_noise_2.pkl','rb') as data_noise2:
    sx_sy_2 = pickle.load(data_noise2)

with open('sx_sy_noise_3.pkl','rb') as data_noise3:
    sx_sy_3 = pickle.load(data_noise3)

with open('sx_sy_noise_4.pkl','rb') as data_noise4:
    sx_sy_4 = pickle.load(data_noise4)

with open('sx_sy_noise_5.pkl','rb') as data_noise5:
    sx_sy_5 = pickle.load(data_noise5)

with open('f_t.pkl','rb') as ft:
    f_t = pickle.load(ft)

sx_0 = numpy.array(sx_sy_0[0])
sy_0 = numpy.array(sx_sy_0[1])

sx_1 = numpy.array(sx_sy_1[0])
sy_1 = numpy.array(sx_sy_1[1])

sx_2 = numpy.array(sx_sy_2[0])
sy_2 = numpy.array(sx_sy_2[1])

sx_3 = numpy.array(sx_sy_3[0])
sy_3 = numpy.array(sx_sy_3[1])

sx_4 = numpy.array(sx_sy_4[0])
sy_4 = numpy.array(sx_sy_4[1])

sx_5 = numpy.array(sx_sy_5[0])
sy_5 = numpy.array(sx_sy_5[1])

minus_10_x = sx_1 - sx_0
minus_10_y = sy_1 - sy_0

minus_20_x = sx_2 - sx_0
minus_20_y = sy_2 - sy_0

minus_30_x = sx_3 - sx_0
minus_30_y = sy_3 - sy_0

minus_40_x = sx_4 - sx_0
minus_40_y = sy_4 - sy_0

minus_50_x = sx_5 - sx_0
minus_50_y = sy_5 - sy_0

fig = plt.figure()
plt.plot([row*0.0001 for row in range(3600000,4000000)], sx_0[3600000:4000000])
plt.savefig('gr_sx_0.png')
plt.close()

fig = plt.figure()
plt.plot([row*0.0001 for row in range(3600000,4000000)], sy_0[3600000:4000000])
plt.savefig('gr_sy_0.png')
plt.close()

fig = plt.figure()
plt.plot([row*0.0001 for row in range(3600000,4000000)], sx_5[3600000:4000000])
plt.savefig('gr_sx_5.png')
plt.close()

fig = plt.figure()
plt.plot([row*0.0001 for row in range(3600000,4000000)], sy_5[3600000:4000000])
plt.savefig('gr_sy_5.png')
plt.close()

# --------------------------------- noise ----------------------------------
fig = plt.figure()
plt.ylim(-0.01,0.01)
plt.plot([row*0.0001 for row in range(3600000,4000000)], minus_10_x[3600000:4000000])
plt.savefig('gr_sx_1_minus_0.png')
plt.close()

fig = plt.figure()
plt.ylim(-0.01,0.01)
plt.plot([row*0.0001 for row in range(3600000,4000000)], minus_10_y[3600000:4000000])
plt.savefig('gr_sy_1_minus_0.png')
plt.close()

fig = plt.figure()
plt.ylim(-0.01,0.01)
plt.plot([row*0.0001 for row in range(3600000,4000000)], minus_20_x[3600000:4000000])
plt.savefig('gr_sx_2_minus_0.png')
plt.close()

fig = plt.figure()
plt.ylim(-0.01,0.01)
plt.plot([row*0.0001 for row in range(3600000,4000000)], minus_20_y[3600000:4000000])
plt.savefig('gr_sy_2_minus_0.png')
plt.close()

fig = plt.figure()
plt.ylim(-0.01,0.01)
plt.plot([row*0.0001 for row in range(3600000,4000000)], minus_30_x[3600000:4000000])
plt.savefig('gr_sx_3_minus_0.png')
plt.close()

fig = plt.figure()
plt.ylim(-0.01,0.01)
plt.plot([row*0.0001 for row in range(3600000,4000000)], minus_30_y[3600000:4000000])
plt.savefig('gr_sy_3_minus_0.png')
plt.close()

fig = plt.figure()
plt.ylim(-0.01,0.01)
plt.plot([row*0.0001 for row in range(3600000,4000000)], minus_40_x[3600000:4000000])
plt.savefig('gr_sx_4_minus_0.png')
plt.close()

fig = plt.figure()
plt.ylim(-0.01,0.01)
plt.plot([row*0.0001 for row in range(3600000,4000000)], minus_40_y[3600000:4000000])
plt.savefig('gr_sy_4_minus_0.png')
plt.close()

fig = plt.figure()
plt.ylim(-0.01,0.01)
plt.plot([row*0.0001 for row in range(3600000,4000000)], minus_50_x[3600000:4000000])
plt.savefig('gr_sx_5_minus_0.png')
plt.close()

fig = plt.figure()
plt.ylim(-0.01,0.01)
plt.plot([row*0.0001 for row in range(3600000,4000000)], minus_50_y[3600000:4000000])
plt.savefig('gr_sy_5_minus_0.png')
plt.close()

# ------------------------------------------- noise -------------------------------



omega_noise_1=0.5
omega_noise_2=8
omega_noise_n=160

dt = 0.0001

oms = [omega_noise_1+(omega_noise_2-omega_noise_1)/omega_noise_n*row for row in range(omega_noise_n)]
phazes = [f_t[0][row][1] for row in range(omega_noise_n)]

fig = plt.figure()
plt.ylim(-0.1,2*numpy.pi+0.1)
plt.plot(oms, phazes)
plt.savefig('phaze_noise_f.png')
plt.close()

sch = 0
spectrum_cos = []
spectrum_sin = []
for row in range(omega_noise_n):
    t1 = time.time()
    omega_noise_temp = omega_noise_1+(omega_noise_2-omega_noise_1)/omega_noise_n*row
    cos_sum = 0
    sin_sum = 0
    for row_2 in range(4000000):
        cos_sum = cos_sum + (sx_5[row_2]-sx_0[row_2])*numpy.cos(omega_noise_temp*row_2*dt)
        sin_sum = sin_sum + (sx_5[row_2]-sx_0[row_2])*numpy.sin(omega_noise_temp*row_2*dt)
    cos_sum = cos_sum/(2*numpy.pi*4000000)
    sin_sum = sin_sum/(2*numpy.pi*4000000)
    spectrum_cos.append(cos_sum)
    spectrum_sin.append(sin_sum)
    t2 = time.time()
    print((row,t2-t1))


spectrum_cos = numpy.array(spectrum_cos)
spectrum_sin = numpy.array(spectrum_sin)

spectrum_pow = []
spectrum_pha = []
for row in range(omega_noise_n):
    spectrum_pow.append(spectrum_cos[row]**2+spectrum_sin[row]**2)
    if spectrum_cos[row]>=0 and spectrum_sin[row]>=0:
        temp_pha = numpy.arctan(spectrum_cos[row]/spectrum_sin[row])
    if spectrum_cos[row]>=0 and spectrum_sin[row]<=0:
        temp_pha = numpy.arctan(spectrum_cos[row]/spectrum_sin[row]) + numpy.pi
    if spectrum_cos[row]<=0 and spectrum_sin[row]>=0:
        temp_pha = numpy.arctan(spectrum_cos[row]/spectrum_sin[row]) + 2*numpy.pi
    if spectrum_cos[row]<=0 and spectrum_sin[row]<=0:
        temp_pha = numpy.arctan(spectrum_cos[row]/spectrum_sin[row]) + numpy.pi
    spectrum_pha.append(temp_pha)

with open('spectrum_noise.pkl','wb') as file_sp:
    pickle.dump((spectrum_cos,spectrum_sin,spectrum_pow,spectrum_pha),file_sp)

fig = plt.figure()
plt.plot(oms, spectrum_pow)
plt.savefig('noise_signal_pow.png')
plt.close()

fig = plt.figure()
plt.plot(oms, spectrum_pha)
plt.savefig('noise_signal_pha.png')
plt.close()

fig = plt.figure()
plt.plot(oms, spectrum_sin)
plt.savefig('noise_signal_sin.png')
plt.close()

fig = plt.figure()
plt.plot(oms, spectrum_cos)
plt.savefig('noise_signal_cos.png')
plt.close()

# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 19:09:09 2020

@author: user
"""

import numpy
import pickle
import mylib
import random
# import matplotlib.pyplot as plt

with open('temp_lp.pkl','rb') as tlp: #load laser parameters
    (o,q,f,a) = pickle.load(tlp)

with open('temp_rc.pkl','rb') as rel: #load relaxsation constants
    (g11,g12,g13,g14,g21,g22,g23,g24,g31,g32,g33,g34,g41,g42,g43,g44,v) = pickle.load(rel)

#with open('temp_dn.pkl','rb') as dtn: #load detones of levels
#    (q1,q2,q3,q4) = pickle.load(dtn)

with open('temp_st.pkl','rb') as stm: #load start state
    temp_start_m = pickle.load(stm)

with open('temp_mp.pkl','rb') as mp:
    (Box, Boy, Boz, Bex, Bey, Bez, fex, fey, fez, oe) = pickle.load(mp)

n=1000000
dt=0.0001

m=1e5
ma=m
mb=m/3

q1=[]
q2=[]
q3=[]
q4=[]

f1=0
f2=2*numpy.pi

omega_noise_1=0.5
omega_noise_2=8
omega_noise_n=160

Ba=0

of=[]
for row in range(omega_noise_n):
    omega_noise_temp = omega_noise_1+(omega_noise_2-omega_noise_1)/omega_noise_n*row
    phaze_noise = random.uniform(f1,f2)
    of.append([omega_noise_temp,phaze_noise])

noise_f=[]
for row in range(n):
    mass_sum = []
    for o_p in of:
        temp_2 = Ba*numpy.sin(o_p[0]*row*dt+o_p[1])*(omega_noise_2-omega_noise_1)/omega_noise_n
        mass_sum.append(temp_2)
    noise_f.append(numpy.sum(mass_sum))



for row in range(n):
    temp_q1=-m*(Boz+Bez*numpy.sin(oe*row*dt+fez)+noise_f[row])
    q1.append(temp_q1)
    temp_q2=m*(Boz+Bez*numpy.sin(oe*row*dt+fez)+noise_f[row])
    q2.append(temp_q2)
    temp_q3=-1/3*m*(Boz+Bez*numpy.sin(oe*row*dt+fez)+noise_f[row])
    q3.append(temp_q3)
    temp_q4=1/3*m*(Boz+Bez*numpy.sin(oe*row*dt+fez)+noise_f[row])
    q4.append(temp_q4)

name_1 = [Boz+Bez*numpy.sin(oe*row*dt+fez) for row in range(n)]

#name_1 = []
#for row in range(n):
#    Boz+Bez*numpy.sin(oe*row*dt+fez)

# plt.figure()
# plt.plot(range(n),noise_f)
# plt.plot(range(n),name_1)
# plt.show()
# plt.close()


name_1 = [Boz+Bez*numpy.sin(oe*row*dt+fez) for row in range(n)]

full_start_matrix = mylib.clc_coherence(temp_start_m[0],temp_start_m[1],(o,q,f,a),(q1[0],q2[0],q3[0],q4[0]),v)
massiv=[full_start_matrix]


Bx=[]
By=[]
for row in range(n):
    Bx1=Box+Bex*numpy.sin(oe*row*dt+fex)
    By1=Boy+Bey*numpy.sin(oe*row*dt+fey)
    Bx.append(Bx1)
    By.append(By1)



for row in range(n):
    p11=massiv[-1][0,0]
    p12=massiv[-1][0,1]
    p13=massiv[-1][0,2]
    p14=massiv[-1][0,3]
    p21=massiv[-1][1,0]
    p22=massiv[-1][1,1]
    p23=massiv[-1][1,2]
    p24=massiv[-1][1,3]
    p31=massiv[-1][2,0]
    p32=massiv[-1][2,1]
    p33=massiv[-1][2,2]
    p34=massiv[-1][2,3]
    p41=massiv[-1][3,0]
    p42=massiv[-1][3,1]
    p43=massiv[-1][3,2]
    p44=massiv[-1][3,3]
    p11c=(1j*(o)*numpy.cos(a)*(numpy.exp(-1j*f)*p41-numpy.exp(1j*f)*p14)+g13*p33+g14*p44+g12*p22-g21*p11)*dt+p11-ma*Bx[row]*p21.imag-ma*By[row]*p21.real
    p12c=(1j*(q2[row]-q1[row])*p12+1j*(o+noise_o[row])*numpy.exp(-1j*f)*(numpy.cos(a)*p42+numpy.sin(a)*p13)-g12*p12)*dt+p12+1j*ma*Bx[row]*(p22-p11)/2-ma*By[row]*(p22-p11)/2
    p22c=(-1j*(o)*numpy.sin(a)*(numpy.exp(1j*f)*p32-numpy.exp(-1j*f)*p23)+g23*p33+g24*p44+g21*p11-g12*p22)*dt+p22+ma*Bx[row]*p21.imag+ma*By[row]*p21.real
    p33c=(1j*(o)*numpy.sin(a)*(numpy.exp(1j*f)*p32-numpy.exp(-1j*f)*p23)-(g13+g23)*p33)*dt+p33-1j*mb*Bx[row]*(p43-p34)/2+mb*By[row]*(p43+p34)/2
    p34c=(1j*(q4[row]-q3[row])*p34-1j*(o+noise_o[row])*numpy.exp(-1j*f)*(numpy.sin(a)*p24+numpy.cos(a)*p31)-(g13/2+g23/2+g14/2+g24/2)*p34)*dt+p34+1j*mb*Bx[row]*(p44-p33)/2-mb*By[row]*(p44-p33)/2
    p44c=(-1j*(o)*numpy.cos(a)*(numpy.exp(-1j*f)*p41-numpy.exp(1j*f)*p14)-(g14+g24)*p44)*dt+p44-1j*mb*Bx[row]*(p43-p34)/2+mb*By[row]*(p43+p34)/2
    p21c=numpy.conjugate(p12c)
    p43c=numpy.conjugate(p34c)
    loc_dow = numpy.matrix([[p11c,p12c],[p21c,p22c]])
    loc_upp = numpy.matrix([[p33c,p34c],[p43c,p44c]])
    loc_full_matrix = mylib.clc_coherence(loc_dow,loc_upp,(o,q,f,a),(q1[row],q2[row],q3[row],q4[row]),v)
    massiv.append(loc_full_matrix)

#with open('temp_res.pkl','wb') as res:
#    pickle.dump(massiv,res)

times = [dt*row for row in range(n+1)]

with open('f_t.pkl', 'wb') as shum:
    pickle.dump((of,noise_f),shum)
    
# with open('o_t.pkl', 'wb') as shum:
#     pickle.dump((of_for_o,noise_o),shum)

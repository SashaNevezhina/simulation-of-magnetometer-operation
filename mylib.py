# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 12:45:07 2020

@author: ZERATUL
"""

import numpy

def clc_coherence(dow,upp,laser,levels,v):
    (o,q,f,a) = laser
    (q1,q2,q3,q4) = levels
    p11o = dow[0,0]
    p12o = dow[0,1]
    p21o = dow[1,0]
    p22o = dow[1,1]
    p33o = upp[0,0]
    p34o = upp[0,1]
    p43o = upp[1,0]
    p44o = upp[1,1]
    p13o = o*(numpy.cos(a)*numpy.exp(-1j*f)*p43o+numpy.sin(a)*numpy.exp(1j*f)*0)/(q+q1-q3-(1j*v))
    p24o = o*(numpy.cos(a)*numpy.exp(-1j*f)*p21o+numpy.sin(a)*numpy.exp(1j*f)*p34o)/(q+q2-q4-(1j*v))
    p14o = o*(numpy.exp(-1j*f)*numpy.cos(a)*(p44o-p11o))/(q+q1-q4-(1j*v))
    p23o = o*(-numpy.exp(1j*f)*numpy.sin(a)*(p33o-p22o))/(q+q2-q3-(1j*v))
    p31o = numpy.conjugate(p13o)
    p42o = numpy.conjugate(p24o)
    p41o = numpy.conjugate(p14o)
    p32o = numpy.conjugate(p23o)
    full_matrix = [[p11o,p12o,p13o,p14o],[p21o,p22o,p23o,p24o],[p31o,p32o,p33o,p34o],[p41o,p42o,p43o,p44o]]
    return numpy.matrix(full_matrix)

def number_name(row):
    if row < 10:
        rowstr = '00' + str(row)
    if row < 100 and row > 9:
        rowstr = '0' + str(row)
    if row > 99:
        rowstr = str(row)
    return rowstr

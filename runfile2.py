# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 19:18:30 2020

@author: user
"""

import numpy
import pickle
import sys
import time
import imp

with open('temp_st.pkl','wb') as stm: #dump start state
    loc_dow = numpy.matrix([[1/2,0],[0,1/2]])
    loc_upp = numpy.matrix([[0,0],[0,0]])
    pickle.dump((loc_dow,loc_upp),stm)

with open('temp_rc.pkl','wb') as rel: #dump relaxsation constants
    g11=False
    g12=0.1
    g13=10
    g14=20
    g21=0.1
    g22=False
    g23=20
    g24=10
    g31=False
    g32=False
    g33=False
    g34=False
    g41=False
    g42=False
    g43=False
    g44=False
    v=100
    pickle.dump((g11,g12,g13,g14,g21,g22,g23,g24,g31,g32,g33,g34,g41,g42,g43,g44,v),rel)


with open('temp_lp.pkl', 'wb') as las:
    o=2
    q=0
    f=0
    a=0
    pickle.dump((o, q, f, a), las)

sch = 1
diamx=[]
diamy=[]
centerx=[]
centery=[]
sumx=[]
sumy=[]


t1=time.time()
with open('temp_mp.pkl', 'wb') as mp:
    oe=2
    Box=1e-10
    Boy=0
    Boz=1e-5
    Bex=0
    Bey=0
    Bez=1.5e-5
    fex=0
    fey=0
    fez=0
    pickle.dump((Box,Boy,Boz,Bex,Bey,Bez,fex,fey,fez,oe),mp)
if 'calculation' not in sys.modules:
    import calculation
else:
    imp.reload(calculation)
sx=[]
for row in range(calculation.n):
    p34=calculation.massiv[row][2,3]
    p21=calculation.massiv[row][1,0]
    sx1=p34.real+p21.real
    sx.append(sx1)
sy=[]
for row in range(calculation.n):
    p34=calculation.massiv[row][2,3]
    p21=calculation.massiv[row][1,0]
    sy1=p34.imag+p21.imag
    sy.append(sy1)
with open('sx_sy_noise.pkl', 'wb') as zapis_v_fail:
    pickle.dump((sx,sy),zapis_v_fail)
T=2*numpy.pi/oe
qt=int(T/calculation.dt)
dn=calculation.n-qt
summex=numpy.sum(numpy.array(sx[dn:calculation.n]))/qt
sumx.append(summex)
summey=numpy.sum(numpy.array(sy[dn:calculation.n]))/qt
sumy.append(summey)
min_value_x = numpy.min(sx[dn:calculation.n])
max_value_x = numpy.max(sx[dn:calculation.n])
min_value_y = numpy.min(sy[dn:calculation.n])  
max_value_y = numpy.max(sy[dn:calculation.n])
diamx.append(max_value_x-min_value_x)
diamy.append(max_value_y-min_value_y)
centerx.append((max_value_x+min_value_x)/2)
centery.append((max_value_y+min_value_y)/2)
t2=time.time()
if sch == 1:
    print(t2-t1)
    sch = 0
 
with open ('diamcenter_noise.pkl','wb') as dc:
    pickle.dump((diamx,diamy,centerx,centery,sumx,sumy),dc)

# -*- coding: utf-8 -*-
"""
Created on Sat May 16 15:36:18 2020

@author: ZERATUL
"""

import pickle
import matplotlib.pyplot as plt

with open("sx_sy_noise.pkl", "rb") as sxsy_n:
    (sx_load_n,sy_load_n)=pickle.load(sxsy_n)

with open("sx_sy.pkl", "rb") as sxsy:
    (sx_load,sy_load)=pickle.load(sxsy)


#fig1=plt.figure()
#plt.plot(range(1000000), sx_load)
#plt.show()
#plt.close()

rep_noise = []
for row in range(100000):
    rep_noise.append(sx_load_n[row]-sx_load[row])

fig2=plt.figure()
plt.plot(range(100000), rep_noise)
plt.savefig('new_fig.jpg')
plt.close()
